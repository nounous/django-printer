Interfaces utilisateur⋅rices
============================

Le site est composé de quatre vues différentes, à savoir
la page d'accueil, la connexion à la Note Kfet, l'impression et
le scan.


Page d'accueil
--------------

La page d'accueil est une page quasi-statique affichant des
informations sur le service et les prix d'impression.

Les prix d'impression affichés sont définis dans les paramètres
du serveur.


Connexion à la Note Kfet
------------------------

La connexion à la Note Kfet est gérée en utilisant le protocole
OAuth2. C'est un protocole qui permet de se connecter à un
serveur distant en utilisant un jeton d'accès.

En souhaitant se connecter, l'utilisateur⋅rice est redirigé⋅e
vers la Note Kfet afin de demander les droits suivants :

* Voir son compte utilisateur⋅rice (club BDE)
* Transférer de l'argent depuis sa propre note en restant positif (club BDE)

Et, dans la mesure du possible, demande les droits suivants :

* Voir les aliases des notes des clubs et des adhérent⋅es du club BDE (club BDE)
* Voir n'importe quel utilisateur⋅rice (club BDE)
* Créer une transaction de ou vers la note d'un club (club Crans)
* Créer n'importe quelle transaction (club BDE)
* Voir sa propre note d'utilisateur (club BDE)
* Créer une transaction quelconque tant que la source reste au-dessus de -50 € (club BDE)
* Voir n'importe quel utilisateur qui est adhérent⋅e BDE (club BDE)

Si au moins l'une des dernières permissions est disponible, cela
permettra à l'utilisateur⋅rice de facturer une autre personne
pour l'impression.

Si l'utilisateur⋅rice refuse, l'authentification est annulée.

Si l'utilisateur⋅rice accepte, il est redirigé⋅e vers la page
d'accueil avec un jeton permettant au site de récupérer un
jeton d'accès avec les bons droits. Ce jeton permet au site
d'effectuer des requêtes sur l'API de la Note Kfet pour
effectuer des transferts.

Le jeton d'authentification est ensuite stocké dans la
session courante, côté serveur.

Plus d'informations sur le protocole sur la documentation de
la Note Kfet :
`<https://note.crans.org/doc/external_services/oauth2/>`_.


Impression
----------

Cette page n'est accessible uniquement pour les personnes
connecté⋅es.

Sur cette page, l'utilisateur⋅rice peut sélectionner ses
paramètres d'impression. C'est également ici qu'iel transmet
le document à imprimer. Dans le cadre d'une photocopie, il
est également possible de choisir parmi un fichier récemment
scanné.

Les options proposées sont :

* Quantité, nombre de copies à réaliser
* Format, taille du papier de sortie (A5, A4, A3)
* Orientation, portrait ou paysage
* Couleur, imprimer en couleur ou en noir et blanc
* Recto-verso
* Pages par feuille (1, 2, 4, 6, 9, 16)
* Rangées de pages (exemple : 1-3,5,7-9)
* Impression livret

Si JavaScript est activé, une estimation de coût est
proposée. Elle n'est cependant pas définitive et peut varier
selon la validité des données.

L'impression livret force l'impression paysage recto-verso
sur bord court avec 2 pages par feuille.

Une fois l'impression lancée, le paiement est facturé à
l'utilisateur⋅rice par Note Kfet. Si le paiement est refusé
ou si une erreur est survenue, l'impression est annulée et un
message apparaît.

Si le paiement est passé, un mail de confirmation est envoyé
à l'utilisateur⋅rice, et l'impression est lancée via une
communication avec le serveur d'impression.


Scan
----

Cette page n'est accessible uniquement pour les personnes
connecté⋅es. Le scan est gratuit, mais nécessite une
authentification pour permettre de récupérer le scan.

Sur cette page, l'utilisateur⋅rice peut définir les
différentes options de scan, qui sont les suivantes :

* Nom du profil, nom du document scanné
* Taille du document scanné (A5, A4, A3)
* Résolution en pixels par pouce (75, 150, 200, 300,
  400, 600, 1200)
* Type de document scanné (PDF, JPEG, PXM, TIFF, WICKET4)
* Orientation du document scanné (portrait, paysage)
* Compression (JPEG est recommandé)
* Profondeur de pixels (Monochrome, 8 bits, 24 bits)

Une fois les paramètres choisis, l'instruction est envoyée
à l'imprimante. L'utilisateur⋅rice récupère ensuite un
numéro de raccourci. Iel peut taper ce numéro sur le clavier
de l'imprimante, précédé d'un #. Le scan est ensuite lancé,
soit depuis le bac dédié soit depuis la vitre selon les
documents chargés. Si le format de sortie est PDF, il est
possible de scanner plusieurs documents d'un coup.

En cas de problème de scan, notamment en cas de bourrage
papier, il est possible de recommencer ou d'annuler.

Lorsque le scan est terminé, un serveur dédié récupère le
document et l'enregistre.

L'utilisateur⋅rice peut récupérer ses scans récents, avant
qu'ils ne soient supprimés. Il est également possible de
les imprimer sur la page d'impression, ce qui permet les
photocopies.
