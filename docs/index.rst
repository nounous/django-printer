Documentation de l'impression Crans
===================================


Bienvenue sur la documentation du serveur d'impression du
Crans. Cette documentation vise à la fois à servir d'aide
aux utilisateur⋅rice⋅s du service ainsi qu'à son
administration.

.. toctree::
   :maxdepth: 2
   :caption: Impression Crans

   faq
   ui
   admin
   install
   install-dev