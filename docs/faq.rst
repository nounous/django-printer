Foire aux questions
===================

Qui peut accéder au service ?
-----------------------------

Tout⋅e adhérent⋅e BDE et/ou Crans peut profiter pleinement du
service d'impression. Concernant les adhérent⋅e⋅s Crans qui ne
sont pas adhérent BDE, s'iels existent, le paiement n'est pour
l'instant pas supporté.


Comment imprimer ?
------------------

Il te suffit de te rendre sur `<https://imprimante.crans.org/>`_,
de te connecter via tes identifiants Note Kfet, d'envoyer ton PDF,
de choisir tes paramètres d'impression, puis de tout envoyer à
l'imprimante. Tu seras automatiquement débité, et l'impression
sera lancée. Il te suffira d'aller voir la sortie de l'imprimante
à la bibliothèque universitaire de l'ENS Paris-Saclay, en 0D76.

Le service est accessible en permanence, pendant les horaires
d'ouverture de la bibliothèque uniquement. Les tâches ne sont
pas acceptées en dehors de ces horaires.


Comment scanner ?
-----------------

Pour scanner un document, tu dois tout d'abord te rendre sur
le site `<https://imprimante.crans.org/>`_ et t'authentifier
par Note Kfet. Ne t'inquiète pas, c'est bien gratuit, le but est
de te permettre de récupérer ton document à la fin et de retracer
les connexions en cas de problème (et uniquement en cas de problème).

Une fois connecté⋅e, tu peux aller dans la section "Scanner" et
rentrer tes paramètres de scan. Une fois ceux-ci choisis, tu
peux cliquer sur le bouton « Scanner ». On te donnera un numéro de
raccourci. Tu peux ensuite te rendre devant l'imprimante située en 0D76
dans la bibliothèque et taper ce numéro de raccourci, précédé du caractère #.
Cela aura pour effet de débuter le scan.

Tu as le choix d'utiliser la vitre ou le bac de feuilles pour donner
tes documents. Cependant, note-bien que le bac peut être un peu capricieux
et provoquer des bourrages papier sans raison. Le scan sur la vitre est
certes plus long mais plus sûr et de meilleur qualité. L'interface de
l'imprimante permet de scanner plusieurs pages à la fois, libre à toi de
terminer la tâche quand bon te semble.

Une fois le scan terminé, clique sur le bouton adéquat sur l'imprimante.
Pour le récupérer, tu peux retourner sur le site
`<https://imprimante.crans.org/>`_ où tu retrouveras ton document prêt à
être téléchargé. Attention : pour des raisons de respect de votre vie
privée, les documents ne sont stockés qu'une semaine avant d'être
définitivement détruits.


Comment photocopier ?
---------------------

La photocopie est gérée comme la combinaison d'un scan et d'une
impression. Pour cela, commencez par scanner le document que vous
souhaitez photocopier (voir ci-dessus), puis revenez dans le menu
d'impression. Au lieu de choisir un fichier depuis votre disque,
vous pouvez choisir le document que vous venez de scanner.

La photocopie est facturée au même prix qu'une impression normale.


Comment recharger mon solde d'impression ?
------------------------------------------

Il n'y a pas de solde d'impression à proprement parler.
L'impression est directement facturée sur la Note Kfet, d'où
la nécessité d'être adhérent⋅e BDE.


Combien coûte l'impression ?
-----------------------------

Une feuille A4 est facturée 1 centime, une feuille A3 2 centimes.
L'impression d'une page en noir et blanc coûte 3 centimes tandis
qu'une page en couleur est facturée 11 centimes. Pour être plus
clair, se référer au tableau suivant :

.. list-table:: Coût de l'impression
   :widths: 20 20 20 20 20 20 20
   :header-rows: 1

   * - Prix par feuille imprimée
     - A5 Recto
     - A5 Recto-verso
     - A4 Recto
     - A4 Recto-verso
     - A3 Recto
     - A3 Recto-verso
   * - Noir & Blanc
     - 0.03 €
     - 0.05 €
     - 0.04 €
     - 0.07 €
     - 0.08 €
     - 0.14 €
   * - Couleur
     - 0.07 €
     - 0.13 €
     - 0.12 €
     - 0.23 €
     - 0.23 €
     - 0.44 €

Les tarifs pourront être révisés si nécessité en réunion du
Conseil d'Administration du Crans.


Je n'ai pas assez d'argent sur ma Note.
---------------------------------------

Tu peux aller recharger ta note à la Kfet, où tu pourras payer
par carte, espèces ou chèque. Tu peux aussi faire un virement
au BDE, n'hésite pas à les contacter à l'adresse
`tresorerie.bde@lists.crans.org <tresorerie.bde@lists.crans.org>`_.


Il n'y a plus de papier / plus d'encre.
---------------------------------------

Nous sommes normalement déjà au courant et nous allons très
rapidement intervenir. Nous te préviendrons lorsque tes documents
seront finalement imprimés.

Si le problème persiste, n'hésite pas à nous prévenir par mail
à l'adresse `contact@crans.org <contact@crans.org>`_.


Mon compte a bien été débité, mais l'impression a eu une erreur.
----------------------------------------------------------------

Tu peux nous contacter à l'adresse
`contact@crans.org <contact@crans.org>`_ en nous indiquant
des informations nous permettant d'identifier ton impression
et ce qu'il s'est passé. N'hésite pas à être le plus précis
possible.


J'ai une suggestion
-------------------

Merci ! Tu peux la soumettre directement sur Gitlab :
`<https://gitlab.crans.org/nounous/django-printer/-/issues>`_
ou bien nous envoyer un mail à l'adresse
`contact@crans.org <contact@crans.org>`_.

.. TODO
