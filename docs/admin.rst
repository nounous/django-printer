Administrer le service
======================

Interface d'administration
--------------------------

L'interface d'administration est accessible à l'adresse
`</admin/>`_ Elle permet de voir et de gérer les impressions
et scans en cours et passés.

En production au Crans, l'accès est permis en lecture à tout
le Collège Technique et écriture aux nounous, via une connexion
au LDAP d'administration.
