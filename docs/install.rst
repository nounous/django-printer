Installer le serveur en production
==================================

Cette page détaille comment installer le serveur
d'impression sur un serveur de production,
dédié uniquement à cet usage. On supposera que
le serveur tourne avec un Debian Bullseye à jour.


Ajout des dépôts bullseye-backports
------------------------------------

Le site est conçu pour tourner avec la dernière version
de Django, à savoir Django 3.2 à cette heure. Cette
version n'est disponible que dans bullseye-backports.

Pour activer les backports, il suffit d'ajouter dans le
fichier ``/etc/apt/sources.list`` :

.. code::

   deb     $MIRROR/debian bullseye-backports main contrib

où ``$MIRROR`` est votre miroir Debian favori.


Installation des dépendances nécessaires
----------------------------------------

On s'efforce pour récupérer le plus possible de dépendances via les paquets Debian
plutôt que via ``pip`` afin de faciliter les mises à jour et avoir une installation
plus propre. On peut donc installer tout ce dont on a besoin, depuis bullseye-backports :

.. code:: bash

   $ sudo apt update
   $ sudo apt install -t bullseye-backports --no-install-recommends cups gettext git nginx python3-authlib python3-coverage python3-cups python3-django python3-django-auth-ldap python3-django-crispy-forms python3-django-extensions python3-pypdf2 python3-requests uwsgi uwsgi-plugin-python3

Sans oublier le paquet ``pip`` pour l'affichage qui n'est pas
dans les dépôts Debian :

.. code:: bash

   $ sudo pip3 install crispy-bootstrap5~=0.4

Ces paquets fournissent une bonne base sur laquelle travailler.

Pour les mettre à jour, il suffit de faire ``sudo apt update`` puis ``sudo apt upgrade``.


Téléchargement du serveur
-------------------------

Tout comme en développement, on utilise directement le
Gitlab du Crans pour récupérer les sources.

On suppose que l'on veut cloner le projet dans le dossier ``/var/www/printer``.

On clone donc le dépôt en tant que ``www-data`` :

.. code:: bash

   $ sudo -u www-data git clone https://gitlab.crans.org/nounous/django-printer.git /var/www/printer


Configuration du serveur
------------------------

Les paramètres personnalisés sont à placer dans le
fichier ``printer/settings_local.py``. Un fichier de
configuration d'exemple est présent dans
``printer/settings_local.example.py``.

Un point sur les paramètres gérés par le site en plus de
ceux de Django :

.. code:: python

   NOTE_KFET_URL = 'https://note.crans.org'
   NOTE_KFET_CLIENT_ID = 'CHANGE_ME'
   NOTE_KFET_CLIENT_SECRET = 'CHANGE_ME'
   DESTINATION_NOTE_ID = 2088
   DESTINATION_NOTE_ALIAS = 'Crans'

Ces paramètres sont utilisés pour l'authentification
des utilisateur⋅rices. avec la Note Kfet. La note 2088
correspond à la note du club Crans sur la Note Kfet 2020
en production. Les identifiant et secret d'application
sont à récupérer dans la gestion des applications OAuth2
de la Note Kfet.


.. code::

   # These parameters may be useful for testing purposes.
   # This disables the interaction with Note Kfet (except login redirect)
   # and/or the printer.
   # Unless in a testing environment (eg. in a continuous integration or without
   # any additional dependency), you should leave this to False.
   IGNORE_NOTE_KFET = False
   IGNORE_CUPS = False

Ces paramètres sont essentiellement utilisés pour
l'intégration de tests unitaires, afin de les faire tourner
sans serveur d'impression ni Note Kfet. Il ne sont pas à toucher
en production.


.. code:: python

   # This is the common name of the printer that is installed in the CUPS server
  PRINTER_NAME = 'Lexmark_X950_Series'

Il s'agit ici du nom de l'imprimante connu par CUPS. Le nom
donné est celui utilisé par le Crans.


.. code:: python
   # Is contacted for scanning. May be HTTP or HTTPS.
   PRINTER_HTTP_SERVER = 'https://printer.example.com'
   # Certificates may be not verified because they are unverifiable
   # due to obsolete machines.
   CHECK_HTTPS_CERTIFICATE = False

Il s'agit ici de l'adresse de l'imprimante, pour la
contacter pour scan. Elle doit être accessible par votre
machine. Le second paramètre permet d'ignorer la
vérification du certificat HTTPS, qui peut être obsolète
notamment dans le cas de l'imprimante actuelle du Crans,
ou en cas de redirection de ports rendant le nom de domaine
utilisé non valide. Laisser à ``False`` n'empêche pas
une connexion HTTPS chiffrée, elle sera simplement non
vérifiée.


.. code:: python

   # To avoid spam, you can define the maximum scanning jobs a user can request.
   # If set to 0, no limit is applied.
   MAX_SIMULTANEOUS_SCANNING_JOBS = 5

Ce nombre permet de limiter le nombre de tâches de
numérisations simultanées pour un⋅e même utilisateur⋅rice.
Cela évite de pouvoir spammer les tâches. Si vous
ne souhaitez pas de limite, mettez à 0.


.. code:: python

   # This address is the address of the server that will receive the scanned file.
   # This may be allowed in your firewall and contactable by the printer.
   SCANNER_SERVER_ADDRESS = '127.0.0.1'
   SCANNER_SERVER_PORT = 9751

Il s'agit ici de l'adresse et du port du serveur de
réception des scans. Cette adresse et ce port seront
transmis à l'imprimante.


.. code:: python

   # Uncomment and adapt to use a LDAP server for authentication
   # AUTHENTICATION_BACKENDS = ["django_auth_ldap.backend.LDAPBackend"]
   # AUTH_LDAP_SERVER_URI = "ldaps://ldap.example.com:1636/"
   # AUTH_LDAP_CONNECTION_OPTIONS = {
   #     ldap.OPT_X_TLS_REQUIRE_CERT: ldap.OPT_X_TLS_ALLOW,
   #     ldap.OPT_X_TLS_NEWCTX: 0,
   #     ldap.OPT_REFERRALS: 0,
   # }
   # AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,ou=passwd,dc=example,dc=com"

   # AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
   #     "ou=group,dc=example,dc=com",
   #     ldap.SCOPE_SUBTREE,
   #     "(objectClass=posixGroup)",
   # )

   # AUTH_LDAP_GROUP_TYPE = PosixGroupType()

   # AUTH_LDAP_MIRROR_GROUPS = True

   # AUTH_LDAP_USER_FLAGS_BY_GROUP = {
   #     "is_active": "cn=_user,ou=group,dc=example,dc=com",
   #     "is_staff": "cn=_user,ou=group,dc=example,dc=com",
   #     "is_superuser": "cn=_nounou,ou=group,dc=example,dc=com",
   # }

Ce bout de configuration permet de configurer une
intégration LDAP, pour se connecter notamment à l'interface
d'administration. Cette configuration est facultative.

Plus d'informations sur la documentation du module :
`<https://django-auth-ldap.readthedocs.io/en/latest/>`_
