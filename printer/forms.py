from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, Row, HTML, Column
from django import forms
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from .models import PrintableFile, Scan


class PrintableFileForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.HiddenInput(),
    )

    note_id = forms.CharField(
        widget=forms.HiddenInput(),
    )

    file = forms.FileField(
        label=_('file').capitalize(),
        required=False,
        help_text=_("The file to print, in PDF format."),
    )

    scan = forms.ModelChoiceField(
        queryset=Scan.objects.all(),
        label=_('or choose from a recently scanned file').capitalize(),
        required=False,
        help_text=_("If you want to make a copy, you can scan your input "
                    "document first and choose it from the list here."),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['file'].widget.attrs['accept'] = 'application/pdf'
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'username',
            'note_id',
            Fieldset(
                _('file').capitalize(),
                Row(
                    AppendedText(
                        'file',
                        mark_safe('<i class="bi bi-file-earmark-pdf"></i>'),
                        wrapper_class='col-md-6',
                    ),
                    AppendedText(
                        'scan',
                        mark_safe('<i class="bi bi-folder2-open"></i>'),
                        wrapper_class='col-md-6',
                    ),
                ),
            ),
            Fieldset(
                _('print parameters').capitalize(),
                Row(
                    AppendedText(
                        'amount',
                        mark_safe('<i class="bi bi-files"></i>'),
                        wrapper_class='col-md-3'
                    ),
                    AppendedText(
                        'format',
                        mark_safe('<i class="bi bi-textarea-resize"></i>'),
                        wrapper_class='col-md-3',
                    ),
                    AppendedText(
                        'orientation',
                        mark_safe('<i class="bi bi-arrow-clockwise"></i>'),
                        wrapper_class='col-md-3',
                    ),
                    AppendedText(
                        'color',
                        mark_safe('<i class="bi bi-circle-fill" style="color: red;"></i>'),
                        wrapper_class='col-md-3',
                    ),
                ),
                Row(
                    AppendedText(
                        'double_sided',
                        mark_safe('<i class="bi bi-back"></i>'),
                        wrapper_class='col-md-4',
                    ),
                    AppendedText(
                        'pages_per_sheet',
                        mark_safe('<i class="bi bi-layout-split"></i>'),
                        wrapper_class='col-md-4',
                    ),
                    AppendedText(
                        'page_ranges',
                        mark_safe('<i class="bi bi-layout-wtf"></i>'),
                        wrapper_class='col-md-4',
                    ),
                ),
                'booklet',
                Fieldset(
                    _('cost estimation').capitalize(),
                    Row(
                        Column(
                            HTML(
                                _("Pages count:") + " <span id=\"pages-count\">0</span>",
                            ),
                            css_class='col-md-3',
                        ),
                        Column(
                            HTML(
                                _("Printed sheets:") + " <span id=\"printed-sheets\">0</span>",
                            ),
                            css_class='col-md-3',
                        ),
                        Column(
                            HTML(
                                _("Estimated cost for one copy:") + " <span id=\"unit-price-estimation\">0 €</span>",
                            ),
                            css_class='col-md-3',
                        ),
                        Column(
                            HTML(
                                _("Total estimated cost:") + " <span id=\"total-price-estimation\">0 €</span>",
                            ),
                            css_class='col-md-3',
                        ),
                    ),
                )
            ),
        )
        self.helper.add_input(Submit('print', _('Print')))

    def clean(self):
        cleaned_data = super().clean()

        if 'scan' in cleaned_data and cleaned_data['scan']:
            # If the user declared a scanned file, we print from this file instead
            cleaned_data['file'] = cleaned_data['scan'].file
            self.files['file'] = cleaned_data['scan'].file
        elif 'file' not in cleaned_data or not cleaned_data['file']:
            self.add_error('file', _('You must either upload a file or choose a scanned file.'))
            self.add_error('scan', _('You must either upload a file or choose a scanned file.'))

        # In booklet mode, some parameters are imposed
        if 'booklet' in cleaned_data and cleaned_data['booklet']:
            cleaned_data['pages_per_sheet'] = 2
            cleaned_data['orientation'] = 'LANDSCAPE'
            cleaned_data['double_sided'] = 'SHORT'

        return cleaned_data

    class Meta:
        model = PrintableFile
        fields = ('username', 'note_id', 'file', 'scan', 'amount', 'format', 'orientation', 'color',
                  'double_sided', 'pages_per_sheet', 'page_ranges', 'booklet', )


class ScanForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            AppendedText(
                'profile_name',
                mark_safe('<i class="bi bi-chat-left-text"></i>'),
            ),
            Row(
                AppendedText(
                    'size',
                    mark_safe('<i class="bi bi-layout-split"></i>'),
                    wrapper_class='col-md-4',
                ),
                AppendedText(
                    'resolution',
                    mark_safe('<i class="bi bi-textarea-resize"></i>'),
                    wrapper_class='col-md-4',
                ),
                AppendedText(
                    'type',
                    mark_safe('<i class="bi bi-filetype-pdf"></i>'),
                    wrapper_class='col-md-4',
                ),
            ),
            Row(
                AppendedText(
                    'orientation',
                    mark_safe('<i class="bi bi-arrow-clockwise"></i>'),
                    wrapper_class='col-md-4',
                ),
                AppendedText(
                    'compression',
                    mark_safe('<i class="bi bi-magic"></i>'),
                    wrapper_class='col-md-4',
                ),
                AppendedText(
                    'depth',
                    mark_safe('<i class="bi bi-brush"></i>'),
                    wrapper_class='col-md-4',
                ),
            ),
        )
        self.helper.add_input(Submit('scan', _('Scan')))

    class Meta:
        model = Scan
        fields = ('profile_name', 'size', 'resolution',
                  'type', 'orientation', 'compression', 'depth', )
