import sys

import cups
from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from . import signals


class PrinterConfig(AppConfig):
    name = 'printer'
    verbose_name = _('printer')
    cups_connection: cups.Connection = None

    def ready(self):
        testing = len(sys.argv) > 1 and (sys.argv[1] == 'test' or sys.argv[1] == 'makemigrations')

        if not settings.IGNORE_CUPS and not testing:
            # Start CUPS connection and store it.
            # We start it only if we are not in a testing environment.
            self.cups_connection = cups.Connection()

        post_save.connect(signals.print_document, sender='printer.PrintableFile')
