import os

from django.conf import settings
from django.test import TestCase, override_settings
from django.urls import reverse_lazy
from django.utils import timezone

from ..models import AccessToken, PrintableFile, Scan


@override_settings(IGNORE_NOTE_KFET=True, IGNORE_CUPS=True)
class TestPrinter(TestCase):
    def setUp(self) -> None:
        """
        Initialize session with sample data.
        """
        session = self.client.session
        self.token = AccessToken.objects.create(
            access_token='',  # Indicate that this is a testing token
            username='test',
            expires_in=3600,
            expires_at=timezone.now() + timezone.timedelta(hours=1),
        )
        session['token_id'] = self.token.id
        session['data'] = {
            'username': 'test',
            'email': 'test@example.com',
            'note_id': 1,
        }
        session.save()

    def test_load_index(self):
        """
        Try to load index page and ensure that it is correctly rendered, in french and in english.
        """
        response = self.client.get(reverse_lazy('index'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse_lazy('index'), HTTP_ACCEPT_LANGUAGE='en-us')
        self.assertEqual(response.status_code, 200)

    def test_redirect_login(self):
        """
        Ensure that printing page redirects to the login page, which redirects
        to Note Kfet.
        """
        sess = self.client.session
        del sess['token_id']
        del sess['data']
        sess.save()

        response = self.client.get(reverse_lazy('print'))
        self.assertRedirects(response, reverse_lazy('login'), 302, 302)

    def test_redirect_note(self):
        """
        Ensure that login page redirects to Note Kfet.
        """
        sess = self.client.session
        del sess['token_id']
        del sess['data']
        sess.save()

        response = self.client.get(reverse_lazy('login'))
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response['location'].startswith(settings.NOTE_KFET_URL))

    def test_load_printing_page(self):
        """
        Try to load the printing page and ensure that it is loading correctly.
        """
        response = self.client.get(reverse_lazy('print'))
        self.assertEqual(response.status_code, 200)

    def test_send_file(self):
        """
        Try to upload a file through the print page and check that an object appears in the database.
        """
        with open(settings.BASE_DIR / 'printer' / 'tests' / 'default-testpage.pdf', 'rb') as file:
            data = {
                'file': file,
                'amount': 1,
                'format': 'A4',
                'orientation': 'PORTRAIT',
                'color': 'True',
                'double_sided': 'LONG',
                'pages_per_sheet': 1,
                'page_ranges': '',
                'booklet': 'False',
                'username': 'test',
                'note_id': 1,
            }
            response = self.client.post(reverse_lazy('print'), data)

        self.assertRedirects(response, reverse_lazy('print'), 302, 200)

        qs = PrintableFile.objects.filter(username='test')
        self.assertTrue(qs.exists())

        printable_file = qs.get()

        # Check that all parameters are correctly set
        self.assertEqual(printable_file.amount, 1)
        self.assertEqual(printable_file.format, 'A4')
        self.assertEqual(printable_file.orientation, 'PORTRAIT')
        self.assertEqual(printable_file.color, True)
        self.assertEqual(printable_file.double_sided, 'LONG')
        self.assertEqual(printable_file.pages_per_sheet, 1)
        self.assertEqual(printable_file.booklet, False)
        self.assertEqual(printable_file.username, 'test')
        self.assertEqual(printable_file.sender, 'test')
        self.assertEqual(printable_file.note_id, 1)

        # Delete file
        printable_file.file.delete()
        printable_file.delete()

    def test_booklet(self):
        """
        Try to upload a booklet through the print page.
        """
        with open(settings.BASE_DIR / 'printer' / 'tests' / 'default-testpage.pdf', 'rb') as file:
            data = {
                'file': file,
                'amount': 1,
                'format': 'A4',
                'orientation': 'PORTRAIT',
                'color': 'True',
                'double_sided': 'LONG',
                'pages_per_sheet': 1,
                'page_ranges': '1,1-1,1',
                'booklet': 'True',
                'username': 'test',
                'note_id': 1,
            }
            response = self.client.post(reverse_lazy('print'), data)

        self.assertRedirects(response, reverse_lazy('print'), 302, 200)

        qs = PrintableFile.objects.filter(username='test')
        self.assertTrue(qs.exists())

        printable_file = qs.get()
        printable_file.job_id = 1
        printable_file.save()

        # Check that all parameters are correctly set
        self.assertEqual(printable_file.amount, 1)
        self.assertEqual(printable_file.format, 'A4')
        self.assertEqual(printable_file.orientation, 'LANDSCAPE')  # Booklet format is always landscape
        self.assertEqual(printable_file.color, True)
        self.assertEqual(printable_file.double_sided, 'SHORT')  # Booklet format
        self.assertEqual(printable_file.pages_per_sheet, 2)  # Booklet format
        self.assertEqual(printable_file.booklet, True)
        self.assertEqual(printable_file.page_ranges, '1,1-1,1')
        self.assertEqual(printable_file.pages, 3)
        self.assertEqual(printable_file.username, 'test')
        self.assertEqual(printable_file.sender, 'test')
        self.assertEqual(printable_file.note_id, 1)

        # Delete files
        os.remove(printable_file.file.path + '.booklet.pdf')
        printable_file.file.delete()
        printable_file.delete()

    def test_scan(self):
        """
        Load the scan page and try to request a scan.
        """
        response = self.client.get(reverse_lazy('scan'))
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            reverse_lazy('scan'),
            {
                'profile_name': 'test',
                'size': 'A4',
                'resolution': '300',
                'type': 'PDF',
                'orientation': 'PORTRAIT',
                'compression': 'JPEG',
                'depth': '24',
            },
        )
        self.assertRedirects(response, reverse_lazy('scan'), 302, 200)

        qs = Scan.objects.filter(username='test')
        self.assertTrue(qs.exists())

        scan = qs.get()
        self.assertEqual(scan.profile_name, 'test')
        self.assertEqual(scan.size, 'A4')
        self.assertEqual(scan.resolution, 300)
        self.assertEqual(scan.type, 'PDF')
        self.assertEqual(scan.orientation, 'PORTRAIT')
        self.assertEqual(scan.compression, 'JPEG')
        self.assertEqual(scan.depth, 24)
