"""printer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.defaults import bad_request, page_not_found, permission_denied, server_error

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('admin/doc/', include('django.contrib.admindocs.urls'), name='admin-doc'),
    path('admin/', admin.site.urls, name='admin'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('authorize/', views.AuthorizeView.as_view(), name='authorize'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('print/', views.PrinterView.as_view(), name='print'),
    path('scan/', views.ScanView.as_view(), name='scan'),
    path('scan/delete/<int:pk>/', views.ScanDeleteView.as_view(), name='scan_delete'),
    path('autocomplete-note/', views.AutocompleteNoteView.as_view(), name='autocomplete-note'),
]

# In production media are served through NGINX with X-Accel-Redirect
if settings.DEBUG:  # pragma: no cover
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
else:
    urlpatterns.append(re_path(r'^files/prints/(?P<path>.*)',
                               views.PrintedFileView.as_view(), name='print-file'))
    urlpatterns.append(path('files/scans/<str:username>/<str:filename>',
                            views.ScannedFileView.as_view(), name='scan-file'))

handler400 = bad_request
handler403 = permission_denied

# Only displayed in production, when debug mode is set to False
handler404 = page_not_found
handler500 = server_error
