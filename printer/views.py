import datetime
import hashlib
import urllib.parse

from authlib.integrations.base_client import OAuthError
from authlib.integrations.django_client import OAuth
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.decorators.http import condition
from django.views.generic import CreateView, RedirectView, TemplateView, DeleteView
import requests

from .forms import PrintableFileForm, ScanForm
from .models import AccessToken, PrintableFile, Scan


class CheckHourMixin:
    def post(self, request, *args, **kwargs):
        """
        If the university library is closed, we prevent any POST request to avoid
        users that try to print something during the night.
        """
        now = timezone.now().astimezone()
        h, m = now.hour, now.minute
        if not (settings.OPENING_HOURS['open'] <= (h, m) <= settings.OPENING_HOURS['close']):
            raise PermissionDenied(_('The library is currently closed. Please try again while it is open.'))
        return super().post(request, *args, **kwargs)


class IndexView(TemplateView):
    template_name = 'index.html'
    extra_context = {'prices': settings.PRINTING_PRICES}

    @staticmethod
    def etag(request, *args, **kwargs):
        if 'data' not in request.session:
            return hashlib.sha1(f'anonymous-{settings.VERSION}'.encode('utf-8')).hexdigest()
        return hashlib.sha1(f"{request.session['data']['username']}-{settings.VERSION}".encode('utf-8')).hexdigest()

    dispatch = method_decorator(condition(etag_func=lambda request: IndexView.etag(request)))(TemplateView.dispatch)


class LoginView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        oauth = OAuth()
        oauth.register('notekfet')
        redirect_url = self.request.build_absolute_uri(reverse('authorize'))
        return oauth.notekfet.authorize_redirect(self.request,
                                                 redirect_url).url


class AuthorizeView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):  # pragma: no cover
        oauth = OAuth()
        oauth.register('notekfet')
        try:
            token = oauth.notekfet.authorize_access_token(self.request)
        except OAuthError:
            messages.error(self.request, _('Authorization failed'))
            return reverse_lazy('index')

        # Ensure that every required scope is present
        for s in settings.NOTE_KFET_REQUIRED_SCOPES.split():
            if s not in token['scope']:
                messages.error(self.request, _('Missing required scope:') + ' ' + s)
                return reverse('index')

        token = AccessToken.objects.create(
            access_token=token['access_token'],
            refresh_token=token['refresh_token'],
            expires_in=token['expires_in'],
            expires_at=datetime.datetime.fromtimestamp(token['expires_at'],
                                                       datetime.timezone.utc),
            scopes=token['scope'],
        )
        self.request.session['token_id'] = token.id
        self.request.session['data'] = token.fetch_user_data()
        return reverse('index')


class LogoutView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if 'token_id' in self.request.session:
            AccessToken.objects.get(id=self.request.session['token_id']).delete()
        self.request.session.flush()
        return reverse_lazy('index')


class PrinterView(CheckHourMixin, CreateView):
    model = PrintableFile
    form_class = PrintableFileForm
    template_name = 'printer/print.html'
    success_url = reverse_lazy('print')
    extra_context = {'prices': settings.PRINTING_PRICES, 'opening_hours': settings.OPENING_HOURS}

    @staticmethod
    def etag(request, *args, **kwargs):
        if 'data' not in request.session:
            return hashlib.sha1(f'anonymous-{settings.VERSION}'.encode('utf-8')).hexdigest()
        username = request.session['data']['username']
        tag = username
        prints = PrintableFile.objects.filter(Q(sender=username) | Q(username=username))
        tag += '|' + '-'.join(f"{p.id}{p.status[:2]}" for p in prints)
        scans = Scan.objects.filter(username=username).all()
        for scan in scans:
            if scan.status != 'DONE':
                scan.check_status()
        tag += '|' + '-'.join(f"{s.id}{s.status[:2]}" for s in scans)
        tag += '|' + settings.VERSION
        return hashlib.sha1(tag.encode('utf-8')).hexdigest()

    @method_decorator(condition(etag_func=lambda request: PrinterView.etag(request)))
    def dispatch(self, request, *args, **kwargs):
        if 'data' not in request.session:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['data'] = self.request.session['data']
        access_token = AccessToken.objects.get(id=self.request.session['token_id'])
        context['can_transfer_from_other_people'] = \
            any(s in access_token.scopes for s in settings.NOTE_KFET_OPTIONAL_SCOPES.split())

        context['form'].fields['username'].initial = self.request.session['data']['username']
        context['form'].fields['note_id'].initial = self.request.session['data']['note_id']
        # Check scan files
        for s in Scan.objects.filter(username=self.request.session['data']['username'], status='PENDING').all():
            s.check_status()
        # Allow to scan only from personal scans
        qs = Scan.objects.filter(
            username=self.request.session['data']['username'], status='DONE')
        context['form'].fields['scan'].queryset = qs
        context['scanned_file_pages'] = {s.id: s.pages for s in qs.all()}

        # Update status of waiting tasks
        for pf in PrintableFile.objects.filter(
                ~Q(status=PrintableFile.JobState.COMPLETED.name),
                Q(username=self.request.session['data']['username'])
                | Q(sender=self.request.session['data']['username'])
        ).all():
            pf.check_status()

        context['waiting_jobs'] = PrintableFile.objects.filter(
            ~Q(status=PrintableFile.JobState.COMPLETED.name),
            Q(username=self.request.session['data']['username'])
            | Q(sender=self.request.session['data']['username'])
        )

        return context

    def form_valid(self, form):
        printable_file = form.instance
        # TODO Maybe validate these data
        printable_file.username = form.data['username']
        printable_file.note_id = form.data['note_id']
        printable_file.sender = self.request.session['data']['username']

        # Calculate page count
        printable_file.pages = printable_file.calculate_pages_count()
        if not printable_file.pages:
            # Invalid PDF
            form.add_error('file', _('Invalid PDF file'))
            return self.form_invalid(form)

        if printable_file.page_ranges:
            # Compute true page count from page ranges
            # Useful to compute true unit price
            max_pages = printable_file.pages
            pages = 0
            for page_range in printable_file.page_ranges.split(','):
                if '-' in page_range:
                    start, end = page_range.split('-')
                else:
                    start = end = page_range
                if not start.isnumeric() or not end.isnumeric():
                    # Invalid page range
                    form.add_error('page_ranges', _('Invalid page range:') + ' ' + page_range)
                    return self.form_invalid(form)
                start, end = int(start), int(end)
                if not 1 <= start <= max_pages or not 1 <= end <= max_pages:
                    # Pages out of bounds
                    form.add_error('page_ranges',
                                   _('Page ranges must be between the first and the last page:') + ' ' + page_range)
                    return self.form_invalid(form)
                pages += abs(end - start) + 1
            printable_file.pages = pages

        printable_file.unit_price = printable_file.calculate_unit_price()

        # Make note transaction if source is different from destination
        if printable_file.note_id != settings.DESTINATION_NOTE_ID:
            token = AccessToken.objects.get(id=self.request.session['token_id'])
            status_code, raw_msg = printable_file.make_transaction(token)

            # Transaction error
            if status_code != 201:
                error_message = str(_("Error while creating transaction:")) + " "
                if status_code == 403:
                    error_message += str(_("Your Note Kfet token seems to be invalid. Please logout and login again."))
                elif 'detail' in raw_msg:
                    error_message += raw_msg['detail']
                else:
                    error_message += str(raw_msg)
                form.add_error(None, error_message)
                return self.form_invalid(form)

            transaction = raw_msg
            printable_file.transaction_id = transaction['id']

        messages.success(self.request,
                         _("File successfully uploaded. You can now check the output of the printer."))

        # Send confirmation email
        printable_file.send_confirmation_email(token)

        return super().form_valid(form)


class ScanView(CheckHourMixin, CreateView):
    model = Scan
    form_class = ScanForm
    template_name = 'printer/scan.html'
    success_url = reverse_lazy('scan')
    extra_context = {'opening_hours': settings.OPENING_HOURS}

    @staticmethod
    def etag(request, *args, **kwargs):
        if 'data' not in request.session:
            return hashlib.sha1(f'anonymous-{settings.VERSION}'.encode('utf-8')).hexdigest()
        username = request.session['data']['username']
        tag = username
        scans = Scan.objects.filter(username=username)
        tag += '|' + '-'.join(f"{s.id}{s.status[:2]}" for s in scans)
        tag += '|' + settings.VERSION
        return hashlib.sha1(tag.encode('utf-8')).hexdigest()

    @method_decorator(condition(etag_func=lambda request: ScanView.etag(request)))
    def dispatch(self, request, *args, **kwargs):
        if 'data' not in request.session:
            return redirect('login')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['data'] = self.request.session['data']
        token = AccessToken.objects.get(id=self.request.session['token_id'])

        # Check if there are new files
        for s in Scan.objects.filter(username=token.username, status='PENDING').all():
            s.check_status()

        qs = Scan.objects.filter(username=token.username, status='PENDING').all()
        context['active_scan_tasks'] = qs.all()
        qs = Scan.objects.filter(username=token.username, status='DONE')
        context['done_scan_tasks'] = qs.all()
        return context

    def form_valid(self, form):
        scan = form.instance

        token = AccessToken.objects.get(id=self.request.session['token_id'])

        # Ensure that the user is not spamming tasks
        active_scans = Scan.objects.filter(username=token.username, shortcut_id__isnull=False)
        if 0 < settings.MAX_SIMULTANEOUS_SCANNING_JOBS <= active_scans.count():
            messages.warning(self.request,
                             _("You have reached the maximal scan tasks number you can have simultaneously. "
                               "Please achieve your tasks first or cancel the useless ones."))
            return self.form_invalid(form)

        scan.username = token.username

        shortcut_id = scan.create_shortcut()
        if not shortcut_id:
            messages.warning(self.request, _("It seems that you already have a pending scanning "
                                             "task with this profile name, please choose another one."))
            return self.form_invalid(form)

        # Display message for the user
        messages.success(
            self.request,
            format_lazy(
                _("Scan task was launched. You can now go to the printer, "
                  "type #{shortcut_id:d} and log in to begin the scan task."),
                shortcut_id=shortcut_id,
            )
        )

        return super().form_valid(form)


class ScanDeleteView(DeleteView):
    model = Scan
    success_url = reverse_lazy('scan')

    def dispatch(self, request, *args, **kwargs):
        if 'data' not in request.session:
            return redirect('login')
        obj = self.get_object()
        if obj.username != request.session['data']['username']:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class AutocompleteNoteView(View):
    def get(self, request, *args, **kwargs):  # pragma: no cover
        if 'token_id' not in request.session:
            raise PermissionDenied
        if 'search' not in request.GET:
            raise Http404
        access_token = AccessToken.objects.get(id=request.session['token_id'])
        resp = requests.get(settings.NOTE_KFET_URL
                            + '/api/note/consumer/?search=user|club&alias=' + request.GET['search'],
                            headers=access_token.auth_header() | {'Accept': 'application/json'})

        return HttpResponse(
            status=resp.status_code,
            content=resp.content,
            content_type=resp.headers['content-type'],
        )


class PrintedFileView(View):
    """
    Allow only superusers to access printed files in production mode.
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.user.is_superuser:
            raise PermissionDenied

        response = HttpResponse()
        # Content-type will be detected by Nginx
        del response['Content-Type']
        response['X-Accel-Redirect'] = urllib.parse.quote(f"/protected/files/prints/{kwargs['path']}")
        response['X-Accel-Buffering'] = 'yes'
        response['X-Accel-Charset'] = 'utf-8'
        return response


class ScannedFileView(View):
    """
    Superusers can access scanned files in production mode.
    Owners of the file can also access it.
    """
    def dispatch(self, request, *args, **kwargs):
        access_granted = False

        if request.user.is_authenticated or request.user.is_superuser:
            access_granted = True

        if 'data' in request.session:
            if request.session['data']['username'] == kwargs['username']:
                access_granted = True

        if not access_granted:
            raise PermissionDenied

        response = HttpResponse()
        # Content-type will be detected by Nginx
        del response['Content-Type']
        response['X-Accel-Redirect'] = urllib.parse.quote(f"/protected/files/scans/{kwargs['username']}"
                                                          f"/{kwargs['filename']}")
        response['X-Accel-Buffering'] = 'yes'
        response['X-Accel-Charset'] = 'utf-8'
        return response
