from pathlib import Path

# Uncomment if you use LDAP login
# import ldap
# from django_auth_ldap.config import LDAPSearch, PosixGroupType

SECRET_KEY = 'CHANGE_ME'

ALLOWED_HOSTS = ['*']

# You can register here additional optional applications
OPTIONAL_APPS = [
    'django_extensions',
]

# Sample example for a SQLite database
# WARNING: you shouldn't use this in production!
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': Path(__file__).resolve().parent.parent / 'db.sqlite3',
    }
}

# For a postgresql setup, you should use the following configuration
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': 'DB_NAME',
#         'USER': 'DB_USER',
#         'PASSWORD': 'DB_PASSWORD',
#         'HOST': 'DB_HOST',
#         'PORT': '',
#     },
# }

# Change this variable to update the location of printed files
# MEDIA_ROOT = Path(__file__).resolve().parent.parent / 'files'

# Uncomment to configure a SMTP server
# By default, mails are printed to the console
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_USE_TLS = True
# EMAIL_HOST = 'smtp.example.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = 'user'
# EMAIL_HOST_PASSWORD = 'password'
SERVER_EMAIL = 'printer@example.com'
DEFAULT_FROM_EMAIL = f'Printer <{SERVER_EMAIL}>'

NOTE_KFET_URL = 'https://note.crans.org'
NOTE_KFET_CLIENT_ID = 'CHANGE_ME'
NOTE_KFET_CLIENT_SECRET = 'CHANGE_ME'
DESTINATION_NOTE_ID = 2088
DESTINATION_NOTE_ALIAS = 'Crans'

# These parameters may be useful for testing purposes.
# This disables the interaction with Note Kfet (except login redirect)
# and/or the printer.
# Unless in a testing environment (eg. in a continuous integration or without
# any additional dependency), you should leave this to False.
IGNORE_NOTE_KFET = False
IGNORE_CUPS = False

# This is the common name of the printer that is installed in the CUPS server
PRINTER_NAME = 'Lexmark_X950_Series'

# Is contacted for scanning. May be HTTP or HTTPS.
PRINTER_HTTP_SERVER = 'https://printer.example.com'
# Certificates may be not verified because they are unverifiable
# due to obsolete machines.
CHECK_HTTPS_CERTIFICATE = False

# To avoid spam, you can define the maximum scanning jobs a user can request.
# If set to 0, no limit is applied.
MAX_SIMULTANEOUS_SCANNING_JOBS = 5

# This address is the address of the server that will receive the scanned file.
# This may be allowed in your firewall and contactable by the printer.
SCANNER_SERVER_ADDRESS = '127.0.0.1'
SCANNER_SERVER_PORT = 9751

# Uncomment and adapt to use a LDAP server for authentication
# AUTHENTICATION_BACKENDS = ["django_auth_ldap.backend.LDAPBackend"]
# AUTH_LDAP_SERVER_URI = "ldaps://ldap.example.com:1636/"
# AUTH_LDAP_CONNECTION_OPTIONS = {
#     ldap.OPT_X_TLS_REQUIRE_CERT: ldap.OPT_X_TLS_ALLOW,
#     ldap.OPT_X_TLS_NEWCTX: 0,
#     ldap.OPT_REFERRALS: 0,
# }
# AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,ou=passwd,dc=example,dc=com"

# AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
#     "ou=group,dc=example,dc=com",
#     ldap.SCOPE_SUBTREE,
#     "(objectClass=posixGroup)",
# )

# AUTH_LDAP_GROUP_TYPE = PosixGroupType()

# AUTH_LDAP_MIRROR_GROUPS = True

# AUTH_LDAP_USER_FLAGS_BY_GROUP = {
#     "is_active": "cn=_user,ou=group,dc=example,dc=com",
#     "is_staff": "cn=_user,ou=group,dc=example,dc=com",
#     "is_superuser": "cn=_nounou,ou=group,dc=example,dc=com",
# }
