# -*- mode: python; coding: utf-8 -*-
# Copyright (C) 2017-2021 by BDE ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later
import os.path
import subprocess
from datetime import datetime
from enum import Enum
from math import ceil
from typing import Optional

import cups
import requests
from PyPDF2 import PdfFileReader, PdfFileWriter
from authlib.integrations.django_client import OAuth
from django import apps
from django.conf import settings
from django.core.mail import send_mail
from django.core.validators import RegexValidator
from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.translation import gettext_lazy as _, activate, deactivate, gettext


class PrintableFile(models.Model):
    """
    A file that has been printed.
    """

    class JobState(Enum):
        """
        Job states that are known by CUPS.
        """
        PENDING = cups.IPP_JOB_PENDING
        HELD = cups.IPP_JOB_HELD
        PROCESSING = cups.IPP_JOB_PROCESSING
        STOPPED = cups.IPP_JOB_STOPPED
        CANCELED = cups.IPP_JOB_CANCELED
        ABORTED = cups.IPP_JOB_ABORTED
        COMPLETED = cups.IPP_JOB_COMPLETED

    file = models.FileField(
        verbose_name=_("file"),
        upload_to='prints/%Y/%m/%d/',
        help_text=_("The file to print, in PDF format."),
    )

    date = models.DateTimeField(
        verbose_name=_("date"),
        default=timezone.now,
        help_text=_("The date and time of the printing."),
    )

    status = models.CharField(
        verbose_name=_("status"),
        max_length=20,
        default='PENDING',
        choices=(
            ('PENDING', _("pending")),
            ('HELD', _("held")),
            ('PROCESSING', _("processing")),
            ('STOPPED', _("stopped")),
            ('CANCELED', _("canceled")),
            ('ABORTED', _("aborted")),
            ('COMPLETED', _("completed")),
        ),
        help_text=_("The current status of the printing."),
    )

    amount = models.IntegerField(
        verbose_name=_("amount"),
        default=1,
        help_text=_("The amount of copies to print."),
    )

    format = models.CharField(
        verbose_name=_("format"),
        max_length=2,
        default='A4',
        choices=(
            ('A3', _("A3")),
            ('A4', _("A4")),
            ('A5', _("A5")),
        ),
        help_text=_("The paper format of the printing."),
    )

    orientation = models.CharField(
        verbose_name=_("orientation"),
        max_length=9,
        default='PORTRAIT',
        choices=(
            ('PORTRAIT', _("portrait")),
            ('LANDSCAPE', _("landscape")),
        ),
        help_text=_("The paper orientation of the printing."),
    )

    color = models.BooleanField(
        verbose_name=_("color"),
        default=False,
        choices=(
            (True, _("Colored")),
            (False, _("Grayscale")),
        ),
        help_text=_("Whether the printing is colored or not. Might affect the printing cost."),
    )

    double_sided = models.CharField(
        verbose_name=_("double sided"),
        max_length=5,
        default='RECTO',
        choices=(
            ('LONG', _("Long-side")),
            ('SHORT', _("Short-side")),
            ('RECTO', _("Recto")),
        ),
        help_text=_("Whether the printed file should be double sided or not. "
                    "The chosen side is the side that the printer will choose to turn the sheet. In general, "
                    "choose the long side for portrait printings and the short side for landscape printings. "
                    "Might affect the printing cost."),
    )

    pages_per_sheet = models.PositiveIntegerField(
        verbose_name=_("pages per sheet"),
        default=1,
        choices=(
            (1, _("1")),
            (2, _("2")),
            (4, _("4")),
            (6, _("6")),
            (9, _("9")),
            (16, _("16")),
        ),
        help_text=_("The number of pages of the input document per printed sheet. Might affect the printing cost."),
    )

    page_ranges = models.CharField(
        verbose_name=_("page ranges"),
        max_length=255,
        blank=True,
        validators=[RegexValidator(r'^\d+(-\d+)?(,\d+(-\d+)?)*$')],
        help_text=_("The page ranges to print. Format: '1-3,5,7-9'. Leave empty to print all pages."),
    )

    booklet = models.BooleanField(
        verbose_name=_("booklet"),
        default=False,
        help_text=_("Check this case if you want to print the document in order to make a booklet."),
    )

    # Useful to track Note Kfet transactions
    note_id = models.PositiveIntegerField(
        verbose_name=_("note id"),
    )

    username = models.CharField(
        verbose_name=_("username"),
        max_length=255,
        help_text=_("Receiver of the printed file"),
    )

    sender = models.CharField(
        verbose_name=_("sender"),
        max_length=255,
        help_text=_("Who printed the file"),
    )

    transaction_id = models.PositiveIntegerField(
        verbose_name=_("transaction id"),
        null=True,  # Null iff the transaction goes to the destination note
    )

    # Useful to get printing price
    pages = models.PositiveIntegerField(
        verbose_name=_("pages"),
        help_text=_("Pages of the printed file"),
    )

    unit_price = models.PositiveIntegerField(
        verbose_name=_("unit price"),
        help_text=_("Total price of one printing, in cents"),
    )

    # Useful when the printing has started to get status
    job_id = models.PositiveIntegerField(
        verbose_name=_("job id"),
        help_text=_("Id of the job on the printer"),
        null=True,
        default=None,
    )

    @property
    def total_price(self) -> int:
        """
        Return the total price of the printing, in cents.
        """
        return self.amount * self.unit_price

    @property
    def total_price_euros(self) -> str:
        """
        Return the total price of the printing, in euros, well-formatted.
        """
        euros, cents = self.total_price // 100, self.total_price % 100
        return f'{euros}.{cents:02d}' if cents else f'{euros}'

    @property
    def unit_price_euros(self) -> str:
        """
        Return the unit price of a printing, in euros, well-formatted.
        """
        euros, cents = self.unit_price // 100, self.unit_price % 100
        return f'{euros}.{cents:02d}' if cents else f'{euros}'

    def calculate_unit_price(self) -> int:
        """
        Calculate the printing price (in cents) according to the settings.
        """
        pages = int(ceil(self.pages / self.pages_per_sheet))

        sheets = pages
        if self.double_sided != 'RECTO':
            sheets = int(ceil(pages / 2))

        # Paper cost, 1 cent per A4 sheet, 2 cents for A3 sheet
        price_per_sheet = settings.PRINTING_PRICES['SHEET'][self.format]
        price_per_file = price_per_sheet * sheets

        # Printing cost
        page_cost = settings.PRINTING_PRICES['COLOR_COPY' if self.color else 'BW_COPY'][self.format]
        price_per_file += page_cost * pages

        return price_per_file

    def make_transaction(self, access_token: "AccessToken") -> tuple[int, dict]:  # pragma: no cover
        """
        Make a transaction on Note Kfet.
        If the transaction was already made, we only fetch it.

        In testing mode, we don't make any transaction, we only fake one.
        """
        if settings.IGNORE_NOTE_KFET:
            # For testing purposes, we don't actually make a transaction.
            return 201, {'id': 1}

        if self.transaction_id:
            # We already have a transaction id, we don't need to make a new one
            resp = requests.get(f'{settings.NOTE_KFET_URL}/api/note/transaction/transaction/{self.transaction_id}/')
            return resp.status_code, resp.json()

        # Create transaction through Note Kfet API
        resp = requests.post(settings.NOTE_KFET_URL + '/api/note/transaction/transaction/',
                             json={
                                 'source': self.note_id,
                                 'source_alias': self.username,
                                 'destination': settings.DESTINATION_NOTE_ID,
                                 'destination_alias': settings.DESTINATION_NOTE_ALIAS,
                                 'reason': 'Impression',
                                 'quantity': self.amount,
                                 'amount': self.unit_price,
                                 'valid': True,
                                 'polymorphic_ctype': settings.NOTE_KFET_TRANSACTION_CTYPE_ID,
                                 'resourcetype': settings.NOTE_KFET_TRANSACTION_CTYPE_NAME,
                             },
                             headers=access_token.auth_header())
        return resp.status_code, resp.json()

    def send_confirmation_email(self, token: Optional["AccessToken"] = None) -> None:
        """
        Send a mail to the user to confirm the printing.
        """
        activate('fr')
        subject_fr = gettext('Printing confirmation')
        activate('en')
        subject_en = gettext('Printing confirmation')
        deactivate()
        subject = f'{subject_fr} // {subject_en}'
        # Get the user's email if available
        email_address = token.fetch_email_address(self.username) \
            if token and token.access_token else settings.SERVER_EMAIL

        message_plain = render_to_string('printer/mails/mail_template.txt', {
            'pf': self,
            'mail_template_fr': 'printer/mails/print_confirmation_fr.txt',
            'mail_template_en': 'printer/mails/print_confirmation_en.txt',
        })
        message_html = render_to_string('printer/mails/mail_template.html', {
            'pf': self,
            'subject': subject,
            'mail_template_fr': 'printer/mails/print_confirmation_fr.html',
            'mail_template_en': 'printer/mails/print_confirmation_en.html',
        })
        send_mail(subject, message_plain, settings.DEFAULT_FROM_EMAIL, [email_address], html_message=message_html)

    def booklet_order(self):
        """
        Generate page order for a booklet printing.
        """

        nop = self.pages
        nop_booklet = int(ceil(nop / 4))

        base = range(2, 2 * nop_booklet + 2, 2)
        pages = []
        for i in base:
            num = nop_booklet * 4 + 1
            pages.append(num - (num - i + 1))
            pages.append(num - i + 1)
            pages.append(num - i)
            pages.append(i)

        return pages

    def replace_with_booklet(self):
        """
        Replace input file with a booklet.
        """
        if not self.booklet:
            return

        writer = PdfFileWriter()

        if self.page_ranges:
            # Compute true page ranges in case of booklet
            order = []
            for page_range in self.page_ranges.split(','):
                if '-' in page_range:
                    start, end = page_range.split('-')
                    order.extend(range(int(start), int(end) + 1))
                else:
                    page = int(page_range)
                    order.append(page)
        else:
            order = list(range(1, self.pages + 1))

        with open(self.file.path, 'rb') as f:
            reader = PdfFileReader(f)

            for page in self.booklet_order():
                if page > self.pages:
                    writer.addBlankPage()
                else:
                    writer.addPage(reader.pages[order[page - 1] - 1])

            with open(self.file.path + ".booklet.pdf", 'wb') as f:
                writer.write(f)

    def calculate_pages_count(self) -> Optional[int]:
        process = subprocess.Popen(['pdfinfo', '-'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        with process.stdin:
            process.stdin.write(self.file.read())
        self.file.seek(0)
        output = process.stdout.read().decode('UTF-8')
        for line in output.split('\n'):
            if line.startswith('Pages:'):
                return int(line.split(':')[1].strip())

    @staticmethod
    def cups_connection() -> cups.Connection:
        """
        Returns the CUPS connection.
        """
        return apps.apps.get_app_config('printer').cups_connection

    @property
    def job_attributes(self):
        """
        Get the CUPS job attributes.
        """
        try:
            return self.cups_connection().getJobAttributes(self.job_id) if not settings.IGNORE_CUPS else {}
        except cups.IPPError as e:
            code, msg = e.args
            if code == 1030:
                # Job not found, maybe cleaned
                return {}
            # Unknown error
            raise

    @property
    def cups_options(self) -> dict:
        """
        Compute CUPS options for a file.
        """
        options = {
            'fit-to-page': 'true',
            'media': self.format,
            'copies': str(self.amount),
            'number-up': str(self.pages_per_sheet),
            'print-color-mode': 'color' if self.color else 'monochrome',
        }

        if self.orientation == 'LANDSCAPE':
            options['landscape'] = 'true'

        if self.double_sided == 'LONG':
            options['sides'] = 'two-sided-long-edge'
        elif self.double_sided == 'SHORT':
            options['sides'] = 'two-sided-short-edge'
        else:
            options['sides'] = 'one-sided'

        if self.page_ranges and not self.booklet:  # page ranges are already managed
            options['page-ranges'] = self.page_ranges

        return options

    def print(self):
        """
        Start the printing.
        """
        if settings.IGNORE_CUPS:
            # For testing purposes, don't start any communication with CUPS.
            return

        cups_connection = self.cups_connection()
        self.job_id = cups_connection.printFile(
            settings.PRINTER_NAME,
            self.file.path + ('.booklet.pdf' if self.booklet else ''),
            self.file.name,
            self.cups_options,
        )
        self.save()

    def check_status(self, force: bool = False) -> None:
        """
        Check the status of the job.
        """
        if settings.IGNORE_CUPS:
            # For testing purposes, don't start any communication with CUPS.
            return

        if self.status == PrintableFile.JobState.COMPLETED.name and not force:
            # Task is already completed, don't query the state
            return

        attributes = self.job_attributes

        if 'job-state' not in attributes:
            # Unknown job, maybe deleted
            return

        self.status = PrintableFile.JobState(attributes['job-state']).name
        self.save()

    def __str__(self):
        return f'{self.file.name}'

    class Meta:
        verbose_name = _("printable file")
        verbose_name_plural = _("printable files")


class Scan(models.Model):
    file = models.FileField(
        verbose_name=_("file"),
        null=True,
        default=None,
        help_text=_("The scanned output file."),
    )

    date = models.DateTimeField(
        verbose_name=_("date"),
        auto_now_add=True,
        help_text=_("The date and time of the scan."),
    )

    username = models.CharField(
        verbose_name=_("username"),
        max_length=255,
        help_text=_("The username of the user who scanned the file."),
    )

    profile_name = models.CharField(
        verbose_name=_("profile name"),
        max_length=255,
        default='scan',
        help_text=_("The name of the output file. You will find it on the screen of the printer."),
        validators=[RegexValidator("^[^/]+$")],
    )

    shortcut_id = models.IntegerField(
        verbose_name=_('shortcut ID'),
        null=True,
        default=None,
    )

    status = models.CharField(
        verbose_name=_("status"),
        max_length=20,
        default='PENDING',
        choices=(
            ('PENDING', _("pending")),
            ('DONE', _("done")),
        ),
        help_text=_("The current status of the scan."),
    )

    size = models.CharField(
        verbose_name=_("size"),
        max_length=20,
        default='A4',
        choices=(
            ('LETTER', _("letter")),
            ('A5', _("A5")),
            ('B5', _("B5")),
            ('A4', _("A4")),
            ('A3', _("A3")),
        ),
        help_text=_("The size of the output file of the scan."),
    )

    resolution = models.IntegerField(
        verbose_name=_("resolution"),
        default=300,
        choices=(
            (75, _("75dpi")),
            (150, _("150dpi")),
            (200, _("200dpi")),
            (300, _("300dpi")),
            (400, _("400dpi")),
            (600, _("600dpi")),
            (1200, _("1200dpi")),
        ),
        help_text=_("The resolution of the output file of the scan. Bigger resolutions make the file bigger."),
    )

    type = models.CharField(
        verbose_name=_("type"),
        max_length=7,
        default='PDF',
        choices=(
            ('PXM', "PXM"),
            ('JPEG', "JPEG"),
            ('TIFF', "TIFF"),
            ('PDF', "PDF"),
            ('WICKET4', "WICKET4"),
        ),
        help_text=_("The type of the output file of the scan. PDF of JPEG files are recommended. "
                    "Only PDF support multiple pages."),
    )

    orientation = models.CharField(
        verbose_name=_("orientation"),
        max_length=9,
        default='PORTRAIT',
        choices=(
            ('PORTRAIT', _("portrait")),
            ('LANDSCAPE', _("landscape")),
        ),
        help_text=_("The orientation of the output file of the scan."),
    )

    compression = models.CharField(
        verbose_name=_("compression"),
        max_length=9,
        default='JPEG',
        choices=(
            ('JPEG', "JPEG"),
            ('ZLIB', "ZLIB"),
            ('G4', "G4"),
            ('CCITT G.4', "CCITT G.4"),
            ('Packbits', "Packbits"),
            ('LZW', "LZW"),
            ('WICKET4', "WICKET4"),
            ('NONE', _("None")),
        ),
        help_text=_("The compression of the output file of the scan. JPEG is recommended."),
    )

    depth = models.IntegerField(
        verbose_name=_("depth"),
        default=24,
        choices=(
            (1, _('black and white')),
            (8, _('8bit')),
            (24, _('24bit')),
        ),
        help_text=_("The number of bits used to store the color of the output file of the scan. "
                    "24 bits are recommended."),
    )

    # TODO Add parameters

    pages = models.PositiveIntegerField(
        verbose_name=_("pages"),
        help_text=_("Pages of the scanned file"),
        default=0,
    )

    def __str__(self) -> str:
        return f'{self.username}:{self.profile_name}'

    @property
    def extension(self) -> str:
        """
        Return the file extension according to the type of scan.
        """
        if self.type == 'PDF':
            return 'pdf'
        elif self.type == 'PXM':
            return 'pnm'
        elif self.type == 'JPEG':
            return 'jpg'
        elif self.type == 'TIFF':
            return 'tif'
        elif self.type == 'WICKET4':
            return 'rgb'
        return ''   # Unknown file type

    @property
    def expected_filename(self) -> str:
        """
        Return the expected filename of the scan.
        """
        return os.path.join('scans', self.username, f'{self.profile_name}.{self.extension}')

    def calculate_pages_count(self) -> Optional[int]:
        process = subprocess.Popen(['pdfinfo', '-'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        with process.stdin:
            process.stdin.write(self.file.read())
        self.file.seek(0)
        output = process.stdout.read().decode('UTF-8')
        for line in output.split('\n'):
            if line.startswith('Pages:'):
                return int(line.split(':')[1].strip())

    def printer_options(self) -> dict:
        """
        Get options that are sent to the scanner.
        """
        return {
            'SCANTYPE': 2,
            'TIME': timezone.now().strftime('%d %B %Y %H:%M:%S %Z'),
            'NAME': f'{self.username}:{self.profile_name}',
            'USERNAME': self.username,
            'PROFILENAME': self.profile_name,
            'ADVCONTRAST': 0,
            'BACKGROUNDREMOVAL': 0,
            'SHADOWDETAIL': 0,
            'MIRRORIMAGE': 0,
            'EDGETOEDGE': 0,
            'NEGATIVEIMAGE': 0,
            'COLORDROPOUT': 0,
            'SHARPNESS': 3,
            'DOCSOURCE': 0,
            'QUICKPROFILE': 0,
            'RESOLUTION': self.resolution,
            'CONTRAST': 'Mixed',
            'DARKNESS': 5,
            'COMPRESSION': self.compression,
            'DEPTH': self.depth,
            'TYPE': self.type,
            'SIZE': self.size,
            'ORIENTATION': self.orientation,
            'DUPLEX': 'SINGLE',
            'MULTIPLEUSE': 'TRUE',
            'KEEPALIVE': 'TRUE',
            'TCPPORT': settings.SCANNER_SERVER_PORT,
            'IPADDRESS': settings.SCANNER_SERVER_ADDRESS,
        }

    def create_shortcut(self) -> Optional[int]:  # pragma: no cover
        """
        Create shortcut on the printer.
        Returns the shortcut ID if the task was successfully created.
        """
        if settings.IGNORE_CUPS:
            return 1

        if self.shortcut_id:
            # Don't create a shortcut if it already exists.
            return self.shortcut_id

        # Initialize task to the printer
        resp = requests.post(
            f'{settings.PRINTER_HTTP_SERVER}/cgi-bin/posttest/printer/netscan/post',
            headers={'Content-Type': 'application/x-www-form-urlencoded'},
            data=self.printer_options(),
            verify=settings.CHECK_HTTPS_CERTIFICATE,
        )
        output = {}
        for line in resp.content.decode('UTF-8').split('\n'):
            if '=' in line:
                k, v = line.split('=', 1)
                output[k] = int(v)

        sid = output['SHORTCUT'] if 'SHORTCUT' in output else None
        self.shortcut_id = sid

        return sid

    def delete_task(self) -> None:  # pragma: no cover
        """
        Delete the shortcut on the printer.
        """
        if settings.IGNORE_CUPS or not self.shortcut_id:
            return

        requests.post(
            f'{settings.PRINTER_HTTP_SERVER}/cgi-bin/posttest/printer/netscan/post',
            headers={'Content-Type': 'application/x-www-form-urlencoded'},
            data={'SCANTYPE': 4, 'NAME': f'{self.username}:{self.profile_name}'},
            verify=settings.CHECK_HTTPS_CERTIFICATE,
        )
        self.shortcut_id = None

    def delete(self, *args, **kwargs) -> None:
        """
        When deleting a task, delete the shortcut on the printer if existing.
        """
        self.delete_task()
        if self.file:
            self.file.delete()
        return super().delete(*args, **kwargs)

    def check_status(self) -> None:
        """
        Check whether the scanned file is arrived or not.
        """
        self.file = self.expected_filename
        if os.path.isfile(self.file.path):
            # Scan exists and delivered
            self.status = 'DONE'
            self.delete_task()
            self.pages = self.calculate_pages_count()
            self.save()

    class Meta:
        verbose_name = _('scan')
        verbose_name_plural = _('scans')


class AccessToken(models.Model):
    username = models.CharField(
        verbose_name=_('username'),
        max_length=255,
    )

    access_token = models.CharField(
        max_length=32,
        verbose_name=_('access token'),
    )

    expires_in = models.PositiveIntegerField(
        verbose_name=_('expires in'),
    )

    scopes = models.CharField(
        max_length=255,
        verbose_name=_('scopes'),
    )

    refresh_token = models.CharField(
        max_length=32,
        verbose_name=_('refresh token'),
    )

    expires_at = models.DateTimeField(
        verbose_name=_('expires at'),
    )

    def refresh(self):  # pragma: no cover
        """
        Refresh the access token.
        """
        oauth = OAuth()
        oauth.register('notekfet')
        # Get the OAuth client
        oauth_client = oauth.notekfet._get_oauth_client()
        # Actually refresh the token
        token = oauth_client.refresh_token(oauth.notekfet.access_token_url,
                                           refresh_token=self.refresh_token,
                                           scope=self.scopes)
        self.access_token = token['access_token']
        self.expires_in = token['expires_in']
        self.scopes = token['scope']
        self.refresh_token = token['refresh_token']
        self.expires_at = timezone.utc.fromutc(
            datetime.fromtimestamp(token['expires_at'])
        )

        self.save()

    def refresh_if_expired(self):
        """
        Refresh the current token if it is invalid.
        """
        if self.expires_at < timezone.now():
            self.refresh()

    def auth_header(self):
        """
        Return HTTP header that contains the bearer access token.
        Refresh the token if needed.
        """
        self.refresh_if_expired()
        return {'Authorization': f'Bearer {self.access_token}'}

    def fetch_user_data(self):  # pragma: no cover
        """
        Extract useful information about the Note Kfet API by using the current
        access token.
        """
        data = requests.get(f'{settings.NOTE_KFET_URL}/api/me/',
                            headers=self.auth_header()).json()

        self.username = data['username']
        self.save()

        return {
            'username': data['username'],
            'email': data['email'],
            'note_id': data['note']['id'],
        }

    def fetch_email_address(self, username: Optional[str] = None) -> str:  # pragma: no cover
        """
        Return the email address of the given note.
        If not available, return the email address of the current user.
        """
        if not username:
            return self.fetch_user_data()['email']

        data = requests.get(f'{settings.NOTE_KFET_URL}/api/user/?username={username}',
                            headers=self.auth_header()).json()['results']
        return data[0]['email'] if data else self.fetch_user_data()['email']

    @classmethod
    def get_token(cls, request):
        return AccessToken.objects.get(pk=request.session['access_token_id'])

    def __str__(self):
        return self.access_token

    class Meta:
        verbose_name = _('access token')
        verbose_name_plural = _('access tokens')
