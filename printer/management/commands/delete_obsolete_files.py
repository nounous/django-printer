from datetime import timedelta

from django.core.management import BaseCommand
from django.db.models import Q
from django.utils import timezone

from ...models import PrintableFile, Scan


class Command(BaseCommand):
    help = 'Delete obsolete files from N days'

    def add_arguments(self, parser):
        parser.add_argument('duration', type=int, default=7, help="Delete files over N days old")
        parser.add_argument('--skip-prints', action='store_true', help="Skip printing files")
        parser.add_argument('--skip-scans', action='store_true', help="Skip scan files")
        parser.add_argument('--purge', '-p', action='store_true', help="Also purge files from database")
        parser.add_argument('--dry-run', '-d', action='store_true', help="Don't actually delete anything")

    def handle(self, *args, **options):
        self.stdout.write("WARNING: printing logs will be removed from the database.")

        date_min = timezone.now() - timedelta(days=options['duration'])

        if not options['skip_prints']:
            # Delete printed files
            for pf in PrintableFile.objects.filter(~Q(file=""), date__lte=date_min).all():
                if options['verbosity'] >= 1:
                    self.stdout.write(self.style.WARNING(
                        f"Deleting printed file {pf.file.name} from {pf.username}..."))
                if not options['dry_run']:
                    pf.file.delete()
                    pf.file = None
                    pf.save()
                    if options['purge']:
                        pf.delete()

            if options['purge']:
                for pf in PrintableFile.objects.filter(file="", date__lte=date_min).all():
                    if options['verbosity'] >= 1:
                        self.stdout.write(self.style.WARNING(
                            f"Deleting printed file from {pf.username} which source was previously deleted..."))
                    if not options['dry_run']:
                        pf.delete()

        if not options['skip_scans']:
            for s in Scan.objects.filter(~Q(file=""), date__lte=date_min).all():
                if options['verbosity'] >= 1:
                    self.stdout.write(self.style.WARNING(
                        f"Deleting scan file {s.file.name} from {s.username}..."))
                for pf in PrintableFile.objects.filter(file=s.file).all():
                    if options['verbosity'] >= 1:
                        self.stdout.write(self.style.WARNING(
                            f"Deleting related printed file from {pf.username}..."))
                    if not options['dry_run']:
                        pf.file = None
                        pf.save()
                if not options['dry_run']:
                    s.file.delete()
                    s.file = None
                    s.save()

            if options['purge']:
                for s in Scan.objects.filter(file="", date__lte=date_min).all():
                    if options['verbosity'] >= 1:
                        self.stdout.write(self.style.WARNING(
                            f"Deleting scanned file from {s.username} which source was previously deleted..."))
                    if not options['dry_run']:
                        s.delete()
