// When a form is submitted, prevent it and display a message instead..
window.addEventListener('load', function() {
    let forms = document.getElementsByTagName('form')
    let modal_node = document.getElementById('night_modal')
    let modal = new bootstrap.Modal(modal_node)

    for (let i = 0; i < forms.length; ++i) {
        let form = forms[i]
        form.addEventListener('submit', function(event) {

            if (!checkDate()) {
                event.preventDefault()
                modal.show()
            }
        })
    }

    // Prevent other modals to be displayed
    let modals = document.getElementsByClassName('modal')
    for (let i = 0; i < modals.length; ++i) {
        let modal = modals[i]
        modal.addEventListener('show.bs.modal', function(event) {
            if (modal.id !== 'night_modal' && !checkDate()) {
                event.preventDefault()
            }
        })
    }
})

function checkDate() {
    /*
    * Ensure that the university library is currently open.
     */
    let opening_hours = JSON.parse(document.getElementById('opening_hours').textContent)

    let date = new Date()
    let open_hours = opening_hours['open']
    open_hours = [("0" + open_hours[0]).slice(-2), ("0" + open_hours[1]).slice(-2)]
    let current_hours = [("0" + date.getHours()).slice(-2), ("0" + date.getMinutes()).slice(-2)]
    let close_hours = opening_hours['close']
    close_hours = [("0" + close_hours[0]).slice(-2), ("0" + close_hours[1]).slice(-2)]

    return open_hours <= current_hours && current_hours <= close_hours;
}
