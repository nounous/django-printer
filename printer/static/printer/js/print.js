let pages_count = 0

let scanned_files_pages = JSON.parse(document.getElementById('scanned_file_pages').textContent)

let note_api_url_obj = document.getElementById('note_api_url_id')

let hidden_username = document.getElementById('id_username');
let hidden_note_id = document.getElementById('id_note_id');

let last_time = -1
let search_obj = document.getElementById("search_user")

search_obj.addEventListener("input", function(event) {
    search_obj.classList.remove("is-valid")
    search_obj.classList.add("is-invalid")

    last_time = event.timeStamp
    let current_time = last_time
    let note_api_url = note_api_url_obj.dataset.url
    fetch(note_api_url + '?search=' + search_obj.value).then(resp => {
        if (current_time !== last_time) {
            // Too late
            return
        }

        resp.json().then(data => {
            let results = document.getElementById("search_user_options")
            let results_html = ''

            for (let result_id in data['results']) {
                let result = data['results'][result_id]
                results_html += '<option value="' + result['name'] + '">'
            }

            results.innerHTML = results_html

            for (let result_id in data['results']) {
                let result = data['results'][result_id]
                if (result['name'] === search_obj.value) {
                    hidden_username.value = result['name']
                    hidden_note_id.value = result['note']['id']

                    search_obj.classList.remove("is-invalid")
                    search_obj.classList.add("is-valid")
                }
            }
        })
    })
});

let file_obj = document.getElementById("id_file")
file_obj.addEventListener('change', function(event) {
    // Read input file to get the number of pages, to give a cost estimation
    let file = event.target.files[0]
    let reader = new FileReader();
    reader.onloadend = function() {
        pages_count = reader.result.match(/\/Type[\s]*\/Page[^s]/g).length;
        refresh_cost_estimation()
    }
    reader.readAsBinaryString(file)
})

document.getElementById('id_amount').addEventListener('input', refresh_cost_estimation)
document.getElementById('id_format').addEventListener('input', refresh_cost_estimation)
document.getElementById('id_color').addEventListener('input', refresh_cost_estimation)
document.getElementById('id_double_sided').addEventListener('input', refresh_cost_estimation)
document.getElementById('id_pages_per_sheet').addEventListener('input', refresh_cost_estimation)
document.getElementById('id_page_ranges').addEventListener('input', refresh_cost_estimation)

function pretty_money (value) {
    if (value % 100 === 0) { return (value < 0 ? '- ' : '') + Math.floor(Math.abs(value) / 100) + ' €' } else {
        return (value < 0 ? '- ' : '') + Math.floor(Math.abs(value) / 100) + '.' +
            (Math.abs(value) % 100 < 10 ? '0' : '') + (Math.abs(value) % 100) + ' €'
    }
}

let prices = JSON.parse(document.getElementById('prices').textContent)

function refresh_cost_estimation() {
    let scan = document.getElementById('id_scan').value
    let amount = document.getElementById('id_amount').value
    let format = document.getElementById('id_format').value
    let color = document.getElementById('id_color').value === "True"
    let double_sided = document.getElementById('id_double_sided').value
    let pages_per_sheet = document.getElementById('id_pages_per_sheet').value
    let page_ranges = document.getElementById('id_page_ranges').value

    let true_page_count = scan === "" ? pages_count : scanned_files_pages[parseInt(scan)]

    if (page_ranges !== '') {
        true_page_count = 0
        let ranges = page_ranges.split(',')
        for (let range_id in ranges) {
            let range = ranges[range_id]
            let range_split = range.split('-')
            if (range_split.length === 2) {
                let a = parseInt(range_split[0])
                let b = parseInt(range_split[1])
                let x = Math.min(a, b)
                let y = Math.max(a, b)
                true_page_count += y - x + 1
            } else if (range_split.length === 1 && range_split[0] !== '') {
                true_page_count += 1
            }
        }
    }

    let pages = Math.ceil(true_page_count / pages_per_sheet)
    let sheets = pages

    if (double_sided !== 'RECTO')
        sheets = Math.ceil(pages / 2)

    let price_per_sheet = format === 'A5' ? prices.SHEET.A5 :
        format === 'A4' ? prices.SHEET.A4 : prices.SHEET.A3
    let price_per_file = price_per_sheet * sheets

    let page_cost = format === 'A5' ? (color ? prices.COLOR_COPY.A5 : prices.BW_COPY.A5) :
    format === 'A4' ? (color ? prices.COLOR_COPY.A4 : prices.BW_COPY.A4) :
    (color ? prices.COLOR_COPY.A3 : prices.BW_COPY.A3)
    price_per_file += page_cost * pages

    document.getElementById("pages-count").innerText = true_page_count
    document.getElementById('printed-sheets').innerText = sheets.toString()
    document.getElementById('unit-price-estimation').innerText = pretty_money(price_per_file)
    document.getElementById('total-price-estimation').innerText = pretty_money(price_per_file * amount)
}

document.getElementById('id_scan').addEventListener('input', function() {
    let scan = document.getElementById('id_scan').value

    if (scan === "")
        file_obj.removeAttribute('disabled')
    else {
        file_obj.setAttribute('disabled', '')
        file_obj.value = ''
    }

    refresh_cost_estimation()
})

let booklet_obj = document.getElementById("id_booklet")
let ds_obj = document.getElementById("id_double_sided")
let orientation_obj = document.getElementById("id_orientation")
let pps_obj = document.getElementById("id_pages_per_sheet")

// When the booklet option is selected, disable the double sided, orientation and pages per sheet options
booklet_obj.addEventListener('change', function() {
    let checked = booklet_obj.checked

    if (checked) {
        ds_obj.setAttribute('disabled', '')
        // Double-sided on short side
        ds_obj.options[1].selected = true

        orientation_obj.setAttribute('disabled', '')
        // Landscape orientation
        orientation_obj.options[1].selected = true

        pps_obj.setAttribute('disabled', '')
        // 2 pages per sheet
        pps_obj.options[1].selected = true
    }
    else {
        ds_obj.removeAttribute('disabled')
        orientation_obj.removeAttribute('disabled')
        pps_obj.removeAttribute('disabled')
    }

    refresh_cost_estimation()
});

if (booklet_obj.checked) {
    booklet_obj.dispatchEvent(new Event('change'))
}

let form_obj = document.getElementsByTagName('form')[0]
let submit_obj = document.getElementById('submit-id-print')

// Open modal when validating the form
submit_obj.dataset.bsToggle = 'modal'
submit_obj.dataset.bsTarget = '#confirm_modal'

// Prevent form submitting, this will be done by the modal
form_obj.addEventListener('submit', event => {
    event.preventDefault()

    document.getElementById('confirm_amount_id').innerText =
        document.getElementById('id_amount').value

    let format_obj = document.getElementById('id_format')
    document.getElementById('confirm_format_id').innerText =
        format_obj.options[format_obj.selectedIndex].text

    let orientation_obj = document.getElementById('id_orientation')
    document.getElementById('confirm_orientation_id').innerText =
        orientation_obj.options[orientation_obj.selectedIndex].text

    let color_obj = document.getElementById('id_color')
    document.getElementById('confirm_color_id').innerText =
        color_obj.options[color_obj.selectedIndex].text

    let double_sided_obj = document.getElementById('id_double_sided')
    document.getElementById('confirm_double_sided_id').innerText =
        double_sided_obj.options[double_sided_obj.selectedIndex].text

    document.getElementById('confirm_pages_per_sheet_id').innerText =
        document.getElementById('id_pages_per_sheet').value

    let page_ranges = document.getElementById('id_page_ranges').value
    document.getElementById('confirm_page_ranges_id').innerText = page_ranges ? page_ranges : '—'

    let booklet_span = document.getElementById('confirm_booklet_id')
    booklet_span.className = booklet_obj.checked ? 'bi bi-check-lg' : 'bi bi-x-lg'

    document.getElementById('confirm_username_id').innerText =
        document.getElementById('id_username').value

    document.getElementById('confirm_total_cost_id').innerText =
        document.getElementById('total-price-estimation').innerText
})

let confirm_button_obj = document.getElementById('confirm_button_id')
confirm_button_obj.addEventListener('click', function() {
    file_obj.removeAttribute('disabled')
    ds_obj.removeAttribute('disabled')
    orientation_obj.removeAttribute('disabled')
    pps_obj.removeAttribute('disabled')
    form_obj.submit()
})
