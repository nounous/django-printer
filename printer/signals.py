def print_document(sender, instance, **kwargs):
    if not instance.job_id:
        # Print document if it has not been printed yet
        if instance.booklet:
            instance.replace_with_booklet()
        instance.print()
