from django.contrib import admin
from django.db.models import Q

from .models import AccessToken, PrintableFile, Scan


@admin.register(PrintableFile)
class PrintableFileAdmin(admin.ModelAdmin):
    list_display = ('date', 'file', 'username', 'sender', 'status', 'format',
                    'double_sided', 'color', 'amount', 'booklet',)
    list_filter = ('status', 'date', 'format', 'double_sided', 'color', 'amount', 'booklet',)
    ordering = ('-date',)
    search_fields = ('file', 'username', 'sender',)
    date_hierarchy = 'date'

    def get_queryset(self, request):
        # Update status of all printable files
        for pf in PrintableFile.objects.filter(~Q(status=PrintableFile.JobState.COMPLETED.name)).all():
            pf.check_status()

        return super().get_queryset(request)


@admin.register(Scan)
class ScanAdmin(admin.ModelAdmin):
    list_display = ('date', 'file', 'username', 'pages', 'status', 'size',
                    'resolution', 'type', 'orientation', 'compression', 'depth',)
    list_filter = ('status', 'date', 'size', 'resolution', 'type', 'orientation', 'compression', 'depth', 'pages',)
    ordering = ('-date',)
    search_fields = ('file', 'username',)
    date_hierarchy = 'date'

    def get_queryset(self, request):
        # Update status of all printable files
        for pf in PrintableFile.objects.filter(~Q(status=PrintableFile.JobState.COMPLETED.name)).all():
            pf.check_status()

        return super().get_queryset(request)


@admin.register(AccessToken)
class AccessTokenAdmin(admin.ModelAdmin):
    list_display = ('access_token', 'expires_at',)
    date_hierarchy = 'expires_at'
