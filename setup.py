#!/usr/bin/env python3

from django.core.management import call_command
from setuptools import setup

call_command('compilemessages')

setup()
